# UPDATE: Unmaintained und half-broken

This depends on Python2 and is therefore half-broken.
I don't want to repair anything that involes Python2, so I stopped maintaining it.

# Installation script for cygdb and simple working example

Because the installation of cygdb is a bit complicated and you sometimes get really unhelpful error messages if you do something wrong, I wrote a script that installs cygdb and a simple working example.
If you still struggle to install cygdb, fell free to contact me at volker.weissmann@gmx.de .

## Installation

### Installation on Arch Linux:

Execute ./installArchLinux.sh

### Installation on other Linux machines:

Install the following programs from source of with your package manager:

 - python2
 - python2-pip
 - gcc
 - make
 - https://github.com/pyenv/pyenv 
 - https://github.com/pkgconf/pkgconf

Optionally install:
 - https://www.gnu.org/software/src-highlite/

Then execute ./installLinux.sh

## Running the example

Execute example/runexample.sh . You should be prompted by something like
```
...lots out ouput...
For help, type "help".
Type "apropos word" to search for commands related to "word"...
Reading symbols from python...
warning: .cygdbinit: No such file or directory
(gdb) 
```
You can then use the commands from [the cython documentation](https://cython.readthedocs.io/en/latest/src/userguide/debugging.html#using-the-debugger) to debug your code. E.g
```
cy break mylib:7
cy run
cy list
cy bt
cy step
cy list
cy step
cy list
cy step
cy list
cy step
cy list
cy step
cy list
```
should step through the code. Note that [cy print is currently broken](https://github.com/cython/cython/issues/2699 "Issue on Github").
