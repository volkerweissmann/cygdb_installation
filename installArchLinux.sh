#!/usr/bin/env bash
#Running this script in Arch Linux will install cygdb
#Maintainer: Volker Weißmann volker.weissmann@gmx.de

set -e

source ${0%/*}/versions.sh

sudo pacman -S python2 python2-pip pyenv gcc make pkg-config source-highlight --noconfirm
sudo pip2 install cython pygments
curl https://ftp.gnu.org/gnu/gdb/gdb-${gdbver}.tar.xz --output gdb.tar.xz
curl https://ftp.gnu.org/gnu/gdb/gdb-${gdbver}.tar.xz.sig --output gdb.tar.xz.sig
curl https://ftp.gnu.org/gnu/gnu-keyring.gpg --output gnu-keyring.gpg
#Quotes from the gpgv manpage:
#gpgv assumes that all keys in the keyring are trustworthy.  That does also mean that it does not check for expired or revoked keys.
#gpgv sigfile [datafile]
#              Verify  the  signature  of the file. The second form is used for detached signatures, where sigfile is the detached signature (either ASCII-armored or binary) and datafile contains the signed data;
#The program returns 0 if everything is fine, 1 if at least one signature was bad, and other error codes for fatal errors.
gpgv --keyring ./gnu-keyring.gpg gdb.tar.xz.sig gdb.tar.xz || exit 1
tar -xf gdb.tar.xz
mv gdb-${gdbver} gdb-python2
cd gdb-python2
mkdir build
cd build
../configure --enable-source-highlight --enable-tui --with-python=python2
make #-j
if pacman -Qi gdb > /dev/null 2>&1 ; then
    sudo pacman -R gdb --noconfirm
fi
sudo make install

pyenv install "$pyver" --debug
eval "$(pyenv init -)"
pyenv shell "$pyver"-debug
python -m pip install cython pygments
echo -e "\nadd-auto-load-safe-path ~/.pyenv/versions/3.8.1-debug/bin/python3.8-gdb.py" >> ~/.gdbinit

echo "finished installation"
