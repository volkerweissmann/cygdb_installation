#!/usr/bin/env bash
cd ~

mkdir -p ~/vagranttest
cd ~/vagranttest
vagrant destroy
rm Vagrantfile
vagrant init archlinux/archlinux
vagrant up
vagrant scp "/home/volker/Sync/DatenVolker/git/cygdb_installation" :cygdb_installation
vagrant ssh -c "sudo pacman -Syy; sudo pacman -S archlinux-keyring --noconfirm; sudo pacman -Syu --noconfirm"
vagrant ssh -c "cygdb_installation/installArchLinux.sh"

