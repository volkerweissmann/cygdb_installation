#!/usr/bin/env python

import mylib

def funcA(x):
	return funcB(x)

def funcB(x):
	return mylib.funcC(x)

print(funcA(3))
