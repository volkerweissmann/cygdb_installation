#!/usr/bin/env false

cpdef funcC(x):
	return funcD(x)

cpdef funcD(x):
	cdef int n = x
	cdef int sum = 0
	for i in range(n):
		sum += i
	return sum
