#!/usr/bin/env python
from setuptools import setup, Extension
from Cython.Build import cythonize

extensions = Extension('mylib', sources=['mylib.pyx'])

setup(
    name = 'mylib',
    url = 'https://gitlab.com/volkerweissmann/cygdb_installation',
    description = 'Example of debugging Cython project',
    author = 'Volker Weißmann',
    author_email = 'volker.weissmann@gmx.de',
    ext_modules = cythonize(
        extensions,
        gdb_debug=True,
    ),
)
